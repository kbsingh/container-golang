FROM registry.centos.org/centos/centos:7

MAINTAINER Karanbir Singh <container-feedback.inb@karan.org>

ENV GOLANG_VER=1.7.4-1.el7

LABEL License=GPLv2
LABEL Version=${GOLANG_VER}

RUN yum -y --setopt=tsflags=nodocs install epel-release && \
    yum -y --setopt=tsflags=nodocs --enablerepo=centosplus \
    --skip-broken install golang\*  && \
    yum -y upgrade && yum clean all

USER 1001

ENTRYPOINT ["/bin/bash"]
